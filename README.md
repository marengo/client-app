# Quasar App (client-app)

A Quasar Framework app

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

kubectl apply -f pod.yml
kubectl get pods
kubectl logs demo
kubectl delete -f pod.yml

docker swarm init
docker service create --name demo alpine:3.5 ping 8.8.8.8
docker service ps demo
docker service logs demo
docker service rm demo

docker images
docker ps -a
docker image build -t client.app:1.0.2 .
docker container run --publish 8000:80 --detach --name bb client.app:1.0.2
http://localhost:8000/#/
docker container rm --force bb

kubectl get po -A

C:\Users\saz\helperProjects\client-app\app.yml
kubectl apply -f app.yml
kubectl get deployments
kubectl get services
localhost:30001
kubectl delete -f app.yml



