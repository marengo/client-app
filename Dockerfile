# develop stage
FROM node:10.16.3 as develop-stage
WORKDIR /app
COPY package*.json ./
RUN npm install --progress=false
RUN npm install -g @quasar/cli
COPY . .
# build stage
FROM develop-stage as build-stage
RUN quasar build
# production stage
FROM nginx:1.16.1 as production-stage
COPY --from=build-stage /app/dist/spa /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
